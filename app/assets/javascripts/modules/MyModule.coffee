$ = require 'jquery'
Template = require '../templates/myModule.hbs'

class MyModule

    constructor: ->

      username = "dauble"
      profileUrl = "http://teamtreehouse.com/" + username + ".json"

      request = $.get(profileUrl, (response) =>
        @displayResponse response
        return
      )

    displayResponse: (response) =>
      $("#badges").html("Total badges: " + response.badges.length);

MyModule

module.exports = MyModule